# Requirements
# Parameters: list of at least two numbers
# Returns: the largest gap between any two consecutive numbers in the list
# The largest gap will always be a positive number


def biggest_gap(numbers):
    max_gap = 0

    for i in range(len(numbers) - 1):
        gap = abs(numbers[i] - numbers[i+1])
        max_gap = max(max_gap, gap)

    return max_gap


test = biggest_gap([1, 3, 6, 10])
print(test)
# or


def biggest_gap(nums):
    max_gap = abs(nums[1] - nums[0])
    for i in range(1, len(nums) - 1):
        gap = abs(nums[i + 1] - nums[i])
        if gap > max_gap:
            max_gap = gap
    return max_gap


test = biggest_gap([-5, 0, 5, 20])
print(test)
