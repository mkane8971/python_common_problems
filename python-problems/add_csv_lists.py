# Requirements
# Input: list of comma-separated string of numbers
# Return: new list with each being corresponding sum
# of the numbers in the CSV

def add_csv_lists(csv_lists):
    result = []
    for csv_string in csv_lists:
        numbers = csv_string.split(',')
        total = sum(int(num) for num in numbers)
        result.append(total)
    return result


test = add_csv_lists(["23,21,3", "4,3,2,34", "3,2", "1", "2,4", "53,42"])
print(test)


# output [47, 43, 5, 1, 6, 95]
