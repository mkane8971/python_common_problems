# Requirements
# Parameters: list of floats or integers between 0 and 100
# Return: An "A" for an average greater than or equal to 90
# A "B" for an average greater than or equal to 80 and less than 90
# A "C" for an average greater than or equal to 70 and less than 80
# A "D" for an average greater than or equal to 60 and less than 70
# An "F" for any other average

def calculate_grade(scores):
    if len(scores) == 0:
        return None
    average = sum(scores) / len(scores)
    if average >= 90:
        return "A"
    elif 80 <= average < 90:
        return "B"
    elif 70 <= average < 80:
        return "C"
    elif 60 <= average < 70:
        return "D"
    else:
        return "F"


scores = calculate_grade([93, 85, 92, 88, 78])
print("The grade is: ", scores)
