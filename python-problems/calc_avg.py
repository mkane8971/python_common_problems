import numpy as np
# Requirements
# Parameters: list of integers or float
# Return: the average of the input


def calculate_average(values):
    if len(values) == 0:
        return None
    else:
        total = sum(values)
        average = total / len(values)
    return average


test = calculate_average([1.5, 2.5, 3.5, 4.5, 5.5])
print("test 1: ", test)

# or


def caculate_average(values):
    if not values:
        return None
    return sum(values) / len(values)


test = calculate_average([1, 2, 3, 4, 5])
print("test 2: ", test)

# or


def calculate_average(values):
    return np.mean(values)


test = calculate_average([2, 4, 6, 8, 15, 18])
print("test 3: ", test)
